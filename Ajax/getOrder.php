<?php
	require '../Model/Database.php';
	require '../Model/Model.php';
	require '../Model/HomePageModel.php';
	
	$value = $_GET['value'];
	$HomePageModel = new HomePageModel;
	$id = $HomePageModel->getIdCustomerByEmail($value)->id;
	$result = $HomePageModel->getOrder($id);
	if(count($result)>0)
	{
		?>
		<h3 class="cart_single_title" style="padding-top:20px;">Discount Cupon</h3>
                    <div class="table-responsive-md">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col">product</th>
                                    <th scope="col">price</th>
                                    <th scope="col">qunatity</th>
                                    <th scope="col">total</th>
                                    <th scope="col">date</th>
                                    <th scope="col">status order</th>
                                    <th scope="col">status pay</th>
                                </tr>
                                <tbody class="data">
                                </tbody>
                            </thead>
                            <?php
                            foreach ($result as $v) {
                            	$list = explode(',',$v->SP);
                            	foreach ($list as $value) {
								list($orderDetail_id,$oder_quantity,$pdd_id,$pdd_color,$pdd_size,$pd_id,$pd_name,$pd_detail,$pd_price,$pd_pricenews,$pd_chatlieu,$pd_xuatxu,$pd_image) = explode('/',$value);
								?>
						                <tr>
						                    <th scope="row">
						                     	   
						                    </th>
						                    <td>
						                        <div class="media">
						                            <div class="d-flex">
						                                <img src="public/img/product/<?=$pd_image?>" width="200px" height="200px" alt="">
						                            </div>
						                            <div class="media-body">
						                                <h6><?=$pd_name?></h6>
						                            </div>
						                        </div>
						                    </td>
						                    <td><p><?=$pd_pricenews?></p></td>
						                    <td><input type="text" placeholder="<?=$oder_quantity?>"></td>
						                    <td><p><?=($pd_pricenews*$oder_quantity)?></p></td>
						                    <td><p><?=$v->date?></p></td>
						                    <td><p><?php
						                    	if($v->status_order == 0)
						                    	{
						                    		?>Đang Chuyển<?php
						                    	}else{
						                    		echo('Hết Hạn Đặt Hàng');
						                    	}
						                    ?></p></td>
						                    <td><p><?php
						                    	if($v->status_pay == 0)
						                    	{
						                    		?>Chưa Thanh Toán<?php
						                    	}else{
						                    		echo('Hết Hạn Thanh Toán');
						                    	}
						                    ?></p></td>
						                </tr>
								<?php
							}
							}
                            ?>
                        </table>
                    </div>
                </div>
		<?php
	
	}else{
		print_r('Khong Tim Thay Du Lieu ...');
	}

?>