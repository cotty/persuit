<?php
	CLass HomePageController extends Controller {
		public function index()
		{
			$controller = new HomePageModel;
			$result = $controller->M_getMenu();
			$menu = $result['menu'];
			$slide = $controller->getSlide();
			$hotproduct = $controller->getHotProduct();
			$top8product = $controller->get8TopProduct();
			$this->view("HomePage",array('menu'=>$menu,'slide'=>$slide,'hotproduct'=>$hotproduct,'top8product'=>$top8product));
		}
		public function detail($id)
		{
			$controller = new HomePageModel;
			$result = $controller->M_getMenu();
			$menu = $result['menu'];
			$product = $controller->getProductById($id);
			$images = $controller->getImageProductById($id);
			$color = $controller->getColorProduct($id);
			$this->view("product-detail",array('menu'=>$menu,'product'=>$product,'images'=>$images,'color'=>$color));
		}
		public function category($id)
		{
			$page = isset($_GET['page'])?$_GET['page']:1;
			$vitri = ($page-1)*3;
			$controller = new HomePageModel;
			$result = $controller->M_getMenu();
			$menu = $result['menu'];
			$typeproduct_byid = $controller->getCategoryById($id);
			$product_bytype = $controller->getProductByTypeProduct($id);
			$product_phantrang = $controller->phantrangProduct($id,$vitri,3);
			$this->view("category",array('menu'=>$menu,'product_bytype'=>$product_bytype,'typeproduct_byid'=>$typeproduct_byid,'product_phantrang'=>$product_phantrang));
		}
		
		public function login()
		{
			if(!isset($_SESSION['user']))
			{
				$controller = new HomePageModel;
				$result = $controller->M_getMenu();
				$menu = $result['menu'];
				$user = $controller->getAllUser();
				if(isset($_POST['btn-login']))
				{
					$userName = $_POST['userName'];
					$password = $_POST['password'];
					foreach ($user as $value) {
						if($userName == $value->username && md5($password) == $value->password)
						{
							$_SESSION['user'] = $controller->getUser($userName,$password);
							$user = $_SESSION['user'];
							
							if($user->quyen == 1)
							{
								sleep(1);
								header('location:index.php?c=admin');
							}else{
								sleep(1);
								header('location:index.php?c=HomePage');
							}
						}
					}
				}

				$this->view("login",array('menu'=>$menu));
			}else{
				header('location:index.php?');
			}
		}
		public function account()
		{
			$controller = new HomePageModel;
			$result = $controller->M_getMenu();
			$menu = $result['menu'];
			$this->view("account",array('menu'=>$menu));
		}
		public function logout()
		{
			if(isset($_SESSION['user']))
			{
				unset($_SESSION['user']);
			}
			header('location:index.php?c=HomePage&a=login');
		}
		public function register()
		{
			$controller = new HomePageModel;
			$result = $controller->M_getMenu();
			$menu = $result['menu'];
			$allprovince = $controller->getAllProvince();
			$customers = $controller->getCustomer();
			$users = $controller->getAllUser();
			if(isset($_POST['register']) && $_POST['token'] == $_SESSION['token'])
			{
				$countEmail =0;
				$countUser =0;
				$name = $_POST['name'];
				$email = $_POST['email'];
				$province = $_POST['province'];
				$district = $_POST['district'];
				$place_detail = $_POST['place_detail'];
				$tel = $_POST['tel'];
				$username = $_POST['username'];
				$password = $_POST['password'];
				foreach ($customers as $ct) {
					if($email == $ct->email)
					{
						$countEmail++;
					}
				}
				foreach ($users as $u) {
					if($username == $u->username)
					{
						$countUser++;
					}
				}
				if($countEmail == 0 && $countUser ==0)
				{
					$customer_id = $controller->insertCustomers($name,$province,$district,$place_detail,$tel,$email);
					$id = $controller->insertUsers($username,$password,$customer_id);
					$_SESSION['user'] =$controller->getUser($username,$password); 
					header('location:index.php');
					unset($_SESSION['error_register']);
				}else{
					if($countEmail != 0)
					{
						$_SESSION['error_register']  = 'email đã tồn tài';
						$_SESSION['error_register_time'] = time();
					}else if($countUser != 0)
					{
						$_SESSION['error_register'] ='User đã tồn tại';
					}else{
						$_SESSION['error_register'] = ' Vui lòng kiểm tra lại !';
					}
				}
			}
			$this->view("register",array('menu'=>$menu,'allprovince'=>$allprovince));
		}
		public function checkout()
		{
			$controller = new HomePageModel;
			$result = $controller->M_getMenu();
			$menu = $result['menu'];
			$list_province = $controller->getAllProvince();
			if(isset($_SESSION['shopping_cart']))
			{
				$listproduct=$_SESSION['shopping_cart'];
				if(isset($_POST['order']))
				{
					if($_POST['token'] == $_SESSION['token'])
					{
						$name = $_POST['name'];
						$province = $_POST['province'];
						$district  = $_POST['district'];
						$place_detail = $_POST['place_detail'];
						$tel = $_POST['tel'];
						$email = $_POST['email'];
						if($name != '' && $province !='' && $place_detail!=''&&$tel != ''&&$email!='')
						{
							$idCustomer = isset($_SESSION['user'])?$_SESSION['user']->custormers_id:$controller->insertCustomer($name,$province,$district,$place_detail,$tel,$email);
							$totalMoney = 0;
							$cart = $_SESSION['shopping_cart'];
							for($a = 0;$a<count($cart);$a++)
							{
								$totalMoney += ($cart[$a]['item_pricenews']*$cart[$a]['item_quantity']);
							}
							$date = date('Y-m-d');
							//insert order
							$idOrder = $controller->inserOrder($idCustomer,$totalMoney,$date);
							if($idOrder)
							{
								$_SESSION['order_success'] = ' Đặt Hàng Thành Công !';
								echo '<script>alert("Đặt Hàng Thành Công  Vui Lòng Kiểm Tra!")</script>';
							}
							for ($i = 0; $i<count($cart);$i++)
							{
								$controller->insertOrderDetail($idOrder,$cart[$i]['item_id'],$cart[$i]['item_quantity']);
							}
							sleep(1);
							unset($_SESSION['shopping_cart']);
							header('location:index.php');
							unset($_SESSION['error_order']);	
						}
					
					}else{
						$_SESSION['error_order'] = ' Vui Lòng Kiểm Tra Lại Thông Tin ';
					}
					
				}
				$this->view("checkout",array('menu'=>$menu,'listproduct'=>$listproduct,'list_province'=>$list_province));
			}else{
				header('location:index.php');
			}
			
		}
		public function orderDetail()
		{
			$controller = new HomePageModel;
			$result = $controller->M_getMenu();
			$menu = $result['menu'];
			$this->view("order-detail",array('menu'=>$menu));
		}
		public function contact()
		{
			$controller = new HomePageModel;
			$result = $controller->M_getMenu();
			$menu = $result['menu'];
			$this->view("contact",array('menu'=>$menu));
		}
	}


?>