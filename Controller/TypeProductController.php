<?php
	Class TypeProductController extends Controller
	{
		public function index()
		{
			$controller = new TypeProductModel;
			$list = $controller->getList();
			$this->view_admin("TypeProduct/list",array('list'=>$list));
		}
		public function add()
		{
			$controller = new TypeProductModel;
			$list = $controller->getList();
			$count = 0;
			$groupproduct = $controller->getListGroupProduct();
			if (isset($_POST['Add'])) { 
				foreach ($list as $lt) {
					if($lt->name == $_POST['Name'])
					{
						$count++;
					}
				}
				if($count == 0)
				{
					$_SESSION['success_add'] = 'Thêm Thành Công !';
					$_SESSION['success_add_time'] = time();
					$this->postAdd($_POST['Name'],$_POST['groupProductId']);
					unset($_SESSION['error_add']);
				}else{
					$_SESSION['error_add'] = " Tên Đã Tồn Tài";
					$_SESSION['error_add_time'] = time();
				}
			}
			$this->view_admin("TypeProduct/add",array('list'=>$list,'groupproduct'=>$groupproduct));
		}
		public function postAdd($name,$idGroupProduct)
		{
			$controller = new TypeProductModel;
			$a = $controller->addModel($name,$idGroupProduct);
			if($a == true)
			{
				header('location:index.php?c=admin&c2=TypeProduct&a=index');
			}else{
				echo 'Thêm Không Thành Công';
			}
			
		}
		public function update($id)
		{
			
			$controller = new TypeProductModel;
			$list = $controller->getList();
			$groupproduct = $controller->getListGroupProduct();
			$infor = $controller->getRowById('TypeProduct',$id);
			$count = 0;
			if(isset($_POST['btnUpdate']))
		    {
				$_SESSION['success_update'] = 'Sửa Thành Công !';
				$this->postUpdate($_POST['txtName'],$_POST['groupProductId'],$id);
				unset($_SESSION['error_update']);
				$_SESSION['success_update_time'] = time();
		    }
			$this->view_admin("TypeProduct/update",array('list'=>$list,'row'=>$infor,'groupproduct'=>$groupproduct));
		}
		public function postUpdate($name,$groupproduct_id,$id)
		{
			$controller = new TypeProductModel;
			$controller->updateModel($name,$groupproduct_id,$id);
			header('location:index.php?c=admin&c2=TypeProduct&a=index');
		}
		public function delete($id)
		{
			$controller = new TypeProductModel;
			$controller->deleteModel($id);
			$_SESSION['delete_TypeProduct'] = 'Xóa Thành Công !';
			$_SESSION['delete_TypeProduct_time'] = time();
			header('location:index.php?c=admin&c2=TypeProduct&a=index');
		}
	}

?>