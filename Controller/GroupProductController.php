<?php
	Class GroupProductController extends Controller
	{
		public function index()
		{
			$controller = new GroupProductModel;
			$list = $controller->getList();
			$this->view_admin("GroupProduct/list",array('list'=>$list));
		}
		public function add()
		{
			$controller = new GroupProductModel;
			$list = $controller->getList();
			$count = 0;
			if (isset($_POST['Add'])) { 
				foreach ($list as $lt) {
					if($lt->name == $_POST['Name'])
					{
						$count++;
					}
				}
				if($count == 0)
				{
					$_SESSION['success_add'] = 'Thêm Thành Công !';
					$_SESSION['success_add_time'] = time();
					$this->postAdd($_POST['Name']);
					unset($_SESSION['error_add']);
				}else{
					$_SESSION['error_add'] = " Tên Đã Tồn Tài";
					$_SESSION['error_add_time'] = time();
				}
			}
			$this->view_admin("GroupProduct/add",array('list'=>$list));
		}
		public function postAdd($name)
		{
			$controller = new GroupProductModel;
			$a = $controller->addModel($name);
			if($a == true)
			{
				header('location:index.php?c=admin&c2=GroupProduct&a=index');
			}else{
				echo 'Thêm Không Thành Công';
			}
			
		}
		public function update($id)
		{
			
			$controller = new GroupProductModel;
			$list = $controller->getList();
			$infor = $controller->getRowById('groupproduct',$id);
			$count = 0;
			if(isset($_POST['btnUpdate']))
		    {
		       foreach ($list as $lt) {
					if($lt->name == $_POST['txtName'])
					{
						$count++;
					}
				}
				if($count == 0)
				{
					$_SESSION['success_update'] = 'Sửa Thành Công !';
					$this->postUpdate($_POST['txtName'],$id);
					unset($_SESSION['error_update']);
					$_SESSION['success_update_time'] = time();
				}else{
					$_SESSION['error_update'] = " Tên Đã Tồn Tài";
					$_SESSION['error_update_time'] = time();
				}
		    }
			$this->view_admin("GroupProduct/update",array('list'=>$list,'row'=>$infor));
		}
		public function postUpdate($name,$id)
		{
			$controller = new GroupProductModel;
			$controller->updateModel($name,$id);
			header('location:index.php?c=admin&c2=GroupProduct&a=index');
		}
		public function delete($id)
		{
			$controller = new GroupProductModel;
			$controller->deleteModel($id);
			$_SESSION['delete_groupproduct'] = 'Xóa Thành Công !';
			$_SESSION['delete_groupproduct_time'] = time();
			header('location:index.php?c=admin&c2=GroupProduct&a=index');
		}
	}

?>