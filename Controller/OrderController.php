<?php
	Class OrderController extends Controller
	{
		public function index()
		{
			$model = new OrderModel;
			$list = $model->getOrder();
			$this->view_admin("Order/list",array('list'=>$list));
		}
		public function update($id)
		{
			$model = new OrderModel;
			$Order_detail = $model->getOrderDetail($id);
			if(isset($_POST['update']))
			{
				$status_order = $_POST['rdo_Order'];
				$status_pay = $_POST['rdo_pay'];
				$model->upDateOrder($id,$status_order,$status_pay);
				sleep(0.5);
				echo('<script>alert("Update thành công !")</script>');
				sleep(0.5);
				header('location:index.php?c=Order');
			}
			$this->view_admin("Order/detail",array('order_detail'=>$Order_detail));
		}
		public function delete($id)
		{
			$model = new OrderModel;
			$model->delete("orders",$id);
			header('index.php?c=Order');
			$_SESSION['delete_order'] = ' Xóa Order Thành Công !';
			$_SESSION['time_delete_order'] = time();
			header('location:index.php?c=Order');
		}
	}

?>