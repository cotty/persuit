<?php
	Class ProductModel extends Model 
	{
		public function listProduct()
		{
			return $this->getAll("Product");
		}
		public function getlistTypeProduct()
		{
			return $this->getAll("TypeProduct");
		}
		public function addProduct($name,$detail,$price,$pricenews,$chatlieu,$xuatxu,$noibat,$typeproduct_id)
		{
			$sql = " INSERT INTO product(name,detail,price,pricenews,chatlieu,xuatxu,noibat,typeproduct_id) VALUES(?,?,?,?,?,?,?,?) ";
			$this->setQuery($sql);
			$this->execute(array($name,$detail,$price,$pricenews,$chatlieu,$xuatxu,$noibat,$typeproduct_id));
			return $this->getLastId();
		}
		public function addProductDetail($size,$color,$quantity,$id)
		{
			$sql = " INSERT INTO product_detail(color,size,product_id,quantity) VALUES (?,?,?,?)";
			$this->setQuery($sql);
			return $this->execute(array($color,$size,$id,$quantity));
		}
		public function addImageProduct($image,$noibat,$id)
		{
			$sql= "INSERT INTO images(image,noibat,product_id) VALUES (?,?,?) ";
			$this->setQuery($sql);
			return $this->execute(array($image,$noibat,$id));
		}
		public function getProductById($id)
		{
			$sql = "SELECT pd.* FROM product pd WHERE pd.id = $id ";
			return $this->getRowHaveCondition($sql);
		}
		public function getProductDetailByIdProduct($id)
		{
			$sql = " SELECT * FROM product_detail pdd WHERE pdd.product_id = $id";
			return $this->getAllRowsHaveCondition($sql);
		}
		public function getImageByIdProduct($id)
		{
			$sql = " SELECT * FROM images WHERE images.product_id = $id";
			return $this->getAllRowsHaveCondition($sql);
		}
		public function updateProduct($id,$name,$detail,$price,$pricenews,$chatlieu,$xuatxu,$noibat,$typeproduct_id)
		{
			$sql =" UPDATE product SET name = ?, detail = ?,price =?,pricenews = ?,chatlieu =?, xuatxu = ?,noibat = ?, typeproduct_id =? WHERE id = ? ";
			$this->setQuery($sql);
			return $this->execute(array($name,$detail,$price,$pricenews,$chatlieu,$xuatxu,$noibat,$typeproduct_id,$id));
		}
		public function updateProductDetail($size,$color,$quantity,$product_id,$id)
		{
			$sql= "UPDATE product_detail SET size = ?, color= ?,quantity = ?, product_id = ? WHERE id = ?";
			$this->setQuery($sql);
			return $this->execute(array($size,$color,$quantity,$product_id,$id));
		}
		public function updateImageProduct($image,$noibat,$product_id,$id)
		{
			$sql= "UPDATE images SET image = ?,noibat = ?, product_id = ? WHERE id = ?";
			$this->setQuery($sql);
			return $this->execute(array($image,$noibat,$product_id,$id));
		}
		public function deleteProduct($id)
		{
			$sql = "DELETE FROM product WHERE id = ?";
			 $this->setQuery($sql);
			 return $this->execute(array($id));
		}
	} 
?>