<?php
	Class Database {
		private $_cursor;
		private $_conn;
		private $_sql;
		public function __construct()
		{
			try {
				$this->_conn = new PDO('mysql:host=localhost;dbname=webshop','root','root');
				$this->_conn->query('SET NAMES "UTF8"');
			} catch (PDOException $e) {
				die($e->getMessage());
			}
		}
		public function setQuery($sql)
		{
			$this->_sql = $sql;
		}
		public function execute($option = array())
		{
			$this->_cursor = $this->_conn->prepare($this->_sql);
			if($option)
			{
				for($i = 0; $i<count($option);$i++)
				{
					$this->_cursor->bindParam($i+1,$option[$i]);
				}
			}
			$this->_cursor->execute();
			return $this->_cursor;
		}
		public function getAllRows($option = array())
		{
			if($option)
			{
				if(!$result = $this->execute($option))
					return false;
			}
			if(!$option)
			{
				if(!$result = $this->execute())
					return false;
			}
			return $result->fetchAll(PDO::FETCH_OBJ);
		}
		public function getRow($option = array())
		{
			if(!$option)
			{
				if(!$result = $this->execute())
					return false;
			}
			if($option)
			{
				if(!$result = $this->execute($option))
				return false;
			}
			return $result->fetch(PDO::FETCH_OBJ);
		}
		public function getLastId()
		{
			return $this->_conn->lastInsertId();
		}

	}


?>