<?php
	
	Class Model extends Database {
		public function getAll($table)
		{
			$sql = "SELECT * FROM $table";
			$this->setQuery($sql);
			return $this->getAllRows();
		}
		public function getAllRowsHaveCondition($sql)
		{
			$this->setQuery($sql);
			return $this->getAllRows();
		}
		public function getRowHaveCondition($sql)
		{
			$this->setQuery($sql);
			return $this->getRow();
		}
		public function getRowById($table,$id)
		{
			$sql = "SELECT * FROM $table WHERE $table.id = $id";
			$this->setQuery($sql);
			return $this->getRow();
		}
		public function Delete($table,$id)
		{
			$sql ="DELETE FROM $table WHERE $table.id = $id";
			$this->setQuery($sql);
			return $this->getRow();
		}
		public function getMenu()
		{
			$sql ="SELECT gp.id,gp.name,GROUP_CONCAT(tp.id,'/',tp.name) as tpp FROM groupproduct gp,typeproduct tp WHERE gp.id = tp.groupproduct_id GROUP BY gp.id";
			$this->setQuery($sql);
			return $this->getAllRows();
		}

	}

?>