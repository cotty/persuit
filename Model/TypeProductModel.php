<?php
Class TypeProductModel extends Model {
	public function getList()
	{
		return $this->getAll("TypeProduct");
	}
	public function addModel($name,$id)
	{
		$sql = " INSERT INTO TypeProduct(name,groupproduct_id) values (?,?)";
		$this->setQuery($sql);
		return $this->execute(array($name,$id));
	}
	public function updateModel($name,$groupproduct_id,$id)
	{
		$sql ="UPDATE TypeProduct SET name = ?,groupproduct_id = ? WHERE id = ?";
		$this->setQuery($sql);
		return $this->execute(array($name,$groupproduct_id,$id));
	}
	public function deleteModel($id)
	{
		$sql =" DELETE FROM TypeProduct WHERE id = ?";
		$this->setQuery($sql);
		return $this->execute(array($id));
	}
	public function getListGroupProduct()
	{
		return $this->getAll("groupproduct");
	}
}
?>