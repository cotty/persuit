<?php
	Class OrderModel extends Model
	{
		public function getOrder()
		{
			$sql = "SELECT * FROM orders";
			return $this->getAllRowsHaveCondition($sql);
		}
		public function getOrderDetail($id)
		{
			$sql = "SELECT orders.*,GROUP_CONCAT(od.id,'/',od.quantity,'/',pdd.id,'/',pdd.color,'/',pdd.size,'/',pdd.product_id,'/',pd.name,'/',pd.pricenews,'/',images.image) as SP FROM orders,images,order_detail as od,product_detail as pdd,product as pd WHERE (orders.id = $id) AND (orders.id = od.orders_id) AND (od.product_detail_id = pdd.id) AND (pdd.product_id = pd.id) AND (pd.id =images.product_id)AND (images.noibat = 1) GROUP BY orders.id";
			return $this->getRowHaveCondition($sql);
		}	
		public function upDateOrder($id,$order,$pay)
		{
			$sql ="UPDATE orders SET status_order = ?, status_pay = ? WHERE id = ?";
			$this->setQuery($sql);
			return $this->execute(array($order,$pay,$id));
		}
	}
	
?>