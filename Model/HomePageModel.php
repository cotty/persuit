<?php
    Class HomePageModel extends Model {
        public function M_getMenu()
        {
            $result = $this->getMenu();
            return array('menu'=>$result);
        }
        public function getSlide()
        {
            $result = $this->getAll('slide');
            return $result;
        }
        public function getHotProduct()
        {
            $sql ="SELECT pt.*,GROUP_CONCAT(img.id,'/',img.image) as img FROM product pt,images img WHERE (pt.id=img.product_id) AND (pt.noibat = 1) AND(img.noibat = 1) group by pt.id";
            return $this->getAllRowsHaveCondition($sql);
        }
        public function getCustomer()
        {
            return $this->getAll('Customers');
        }
        public function insertCustomers($name,$province,$district,$place_detail,$tel,$email)
        {
            $sql = "INSERT INTO customers(Name,id_address_province,id_address_district,address_detail,tel,email,status) VALUES(?,?,?,?,?,?,?) ";
            $this->setQuery($sql);
            $this->execute(array($name,$province,$district,$place_detail,$tel,$email,1));
            return $this->getLastId();
        }
        public function get8TopProduct()
        {
            $sql = "SELECT pt.*,GROUP_CONCAT(img.id,'/',img.image) as img FROM product pt,images img WHERE (pt.id=img.product_id) AND(img.noibat = 1)  group by pt.id order by pt.id DESC  limit 0,8";
        return $this->getAllRowsHaveCondition($sql);
        }
        public function getCategoryById($id)
        {
            return $this->getRowById('typeproduct',$id);
        }
        public function getProductByTypeProduct($id)
        {
            $sql = "SELECT pt.*,GROUP_CONCAT(img.id,'/',img.image) as img FROM product pt,images img WHERE (pt.id=img.product_id) AND (img.noibat = 1) AND (pt.typeproduct_id = $id) group by pt.id";
            return $this->getAllRowsHaveCondition($sql);
        }
        public function phantrangProduct($id,$vitri,$limit)
        {
             $sql = "SELECT pt.*,GROUP_CONCAT(img.id,'/',img.image) as img FROM product pt,images img WHERE (pt.id=img.product_id) AND (img.noibat = 1) AND (pt.typeproduct_id = $id) group by pt.id LIMIT $vitri,$limit";
            return $this->getAllRowsHaveCondition($sql);
        }
        public function getImageProductById($id)
        {
            $sql = "SELECT * FROM images WHERE product_id = $id";
            return $this->getAllRowsHaveCondition($sql);
        }
        public function getProductById($id)
        {
            $sql = "SELECT pd.*,GROUP_CONCAT(pdd.id,'/',pdd.color,'/',pdd.size,'/',pdd.quantity) as pd_detail FROM product pd,product_detail pdd WHERE pd.id=pdd.product_id AND pd.id = $id GROUP BY pd.id";
            return $this->getRowHaveCondition($sql);
        }
        public function getSizeByColor($id,$color)
        {
            $sql = "SELECT * FROM product_detail WHERE product_id = $id AND color = '$color'";
            return $this->getAllRowsHaveCondition($sql);
        }
        public function getColorProduct($id)
        {
            $sql = "SELECT color FROM product_detail WHERE product_id = $id group by color";
            return $this->getAllRowsHaveCondition($sql);
        }
        public function getSoLuongByColorAndSize($id,$color,$size)
        {
            $sql = "SELECT id,quantity FROM product_detail WHERE product_id = $id AND color = '$color' AND size = '$size'";
            return $this->getRowHaveCondition($sql);
        }
        public function getProDuctOrder($id)
        {
            $sql="SELECT pdd.*,pd.name,pd.detail,pd.price,pd.pricenews,pd.chatlieu,pd.xuatxu,img.image FROM product_detail pdd,product pd,images img WHERE (pdd.id = $id) AND(pd.id = img.product_id)  AND (pdd.product_id = pd.id) AND(img.noibat = 1) GROUP BY pdd.id";
            return $this->getRowHaveCondition($sql);
        }
        public function getAllUser()
        {
            return $this->getAll('users');
        }
        public function getUser($name,$password)
        {
            $password = md5($password);
            $sql = "SELECT users.*,customers.Name,address_province.name_province,address_district.Name as name_district,address_detail,customers.tel,customers.email,customers.status FROM users,customers,address_province,address_district where users.username = '$name' AND users.password = '$password' AND users.custormers_id = customers.id AND customers.id_address_province = address_province.id AND customers.id_address_district = address_district.id";
            return $this->getRowHaveCondition($sql);
        }
        public function getInfUser($id)
        {
            $sql = "SELECT * FROM customers WHERE customers.id = $id ";
            return $this->getRowHaveCondition($sql);
        }
        public function getAllProvince()
        {
            return $this->getAll('address_province');
        }
        public function getDistrict($id)
        {
            $sql = " SELECT * FROM address_district WHERE address_province_id = $id";
            return $this->getAllRowsHaveCondition($sql);
        }
        public function insertCustomer($name,$province,$district,$place_detail,$tel,$email)
        {
            $sql  = "INSERT INTO customers(Name,id_address_province,id_address_district,address_detail,tel,email) VALUES (?,?,?,?,?,?)";
            $this->setQuery($sql);
             $this->execute(array($name,$province,$district,$place_detail,$tel,$email));
             return $this->getLastId();
        }
        public function insertUsers($username,$password,$idCustomer)
        {
            $sql= "INSERT INTO users(username,password,custormers_id) values(?,?,?)";
            $this->setQuery($sql);
            $this->execute(array($username,md5($password),$idCustomer));
            return $this->getLastId();
        }
        public function inserOrder($idCustomer,$totalMoney,$date)
        {
            $sql =" INSERT INTO orders(custormers_id,total_money,date ) VALUES (?,?,?) ";
            $this->setQuery($sql);
            $this->execute(array($idCustomer,$totalMoney,$date));
            return $this->getLastId();
        }
        public function insertOrderDetail($idorder,$idproduct_detail,$quantity)
        {
            $sql = "INSERT INTO order_detail(orders_id,product_detail_id,quantity) VALUES (?,?,?)";
            $this->setQuery($sql);
            return $this->execute(array($idorder,$idproduct_detail,$quantity));
        }
        public function getIdCustomerByEmail($email)
        {
            $sql = "SELECT customers.id FROM customers WHERE email = '$email' ";
            return $this->getRowHaveCondition($sql);
        }
        public function getOrder($id)
        {
            $sql = "SELECT orders.*,GROUP_CONCAT(od.id,'/',od.quantity,'/',pdd.id,'/',pdd.color,'/',pdd.size,'/',pdd.product_id,'/',pd.name,'/',pd.detail,'/',pd.price,'/',pd.pricenews,'/',pd.chatlieu,'/',pd.xuatxu,'/',images.image) as SP FROM orders,images,order_detail as od,product_detail as pdd,product as pd WHERE (orders.custormers_id = $id) AND (orders.id = od.orders_id) AND (od.product_detail_id = pdd.id) AND (pdd.product_id = pd.id) AND (pd.id =images.product_id)AND (images.noibat = 1) GROUP BY orders.id";
            return $this->getAllRowsHaveCondition($sql);
        }

    }   

?>