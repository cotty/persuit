-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th8 09, 2018 lúc 09:15 AM
-- Phiên bản máy phục vụ: 10.1.32-MariaDB
-- Phiên bản PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `webshop`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `address_detail`
--

CREATE TABLE `address_detail` (
  `id` int(11) NOT NULL,
  `address_detail` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `address_district`
--

CREATE TABLE `address_district` (
  `id` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `address_province_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `address_district`
--

INSERT INTO `address_district` (`id`, `Name`, `address_province_id`) VALUES
(1, 'Thành phố Long Xuyên', 1),
(2, 'Thành phố Châu Đốc', 1),
(3, 'Thị xã Tân Châu', 1),
(4, 'Huyện Châu Phú', 1),
(5, 'Huyện Phú Tân', 1),
(6, 'Thành phố Vũng Tàu', 2),
(7, 'Thị xã Bà Rịa', 2),
(8, 'Huyện Xuyên Mộc', 2),
(9, 'Huyện Long Điền', 2),
(10, 'Huyện Côn Đảo', 2),
(11, 'Huyện Tân Thành', 2),
(12, 'Huyện Châu Đức', 2),
(13, 'Huyện Đất Đỏ', 2),
(21, 'Thành phố Bắc Giang', 3),
(22, 'Huyện Yên Thế', 3),
(23, 'Huyện Lục Ngạn', 3),
(24, 'Huyện Sơn Động', 3),
(25, 'Huyện Lục Nam', 3),
(26, 'Huyện Tân Yên', 3),
(27, 'Huyện Hiệp Hoà', 3),
(37, 'Quận Ba Đình', 10),
(38, 'Huyện Từ Liêm', 10),
(39, 'Huyện Thanh Trì', 10),
(40, 'Huyện Gia Lâm', 10),
(41, 'Huyện Đông Anh', 10),
(42, 'Huyện Sóc Sơn', 10),
(43, 'Quận Thanh Xuân', 10),
(44, 'Quận Hà Đông', 10),
(45, 'Thị xã Sơn Tây', 10),
(46, 'Huyện Ba Vì', 10),
(52, 'Quận Tây Hồ', 10),
(53, 'Quận Cầu Giấy', 10),
(54, 'Quận Thanh Xuân', 10),
(55, 'Quận Ba Đình', 10),
(56, 'Quận Hoàn Kiếm', 10),
(57, 'Quận Hai Bà Trưng', 10),
(58, 'Quận Đống Đa', 10);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `address_province`
--

CREATE TABLE `address_province` (
  `id` int(11) NOT NULL,
  `name_province` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `address_province`
--

INSERT INTO `address_province` (`id`, `name_province`) VALUES
(1, 'An Giang'),
(2, 'Bà Rịa - Vũng Tàu'),
(3, 'Bắc Giang'),
(4, 'Bạc Liêu'),
(5, 'Bắc Ninh'),
(6, 'Bến Tre'),
(7, 'Bình Định'),
(8, 'Bình Dương'),
(9, 'Bình Phước'),
(10, 'Hà Nội');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `id_address_province` int(11) NOT NULL,
  `id_address_district` int(11) NOT NULL,
  `address_detail` varchar(50) NOT NULL,
  `tel` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `status` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `customers`
--

INSERT INTO `customers` (`id`, `Name`, `id_address_province`, `id_address_district`, `address_detail`, `tel`, `email`, `status`) VALUES
(4, 'Quý', 10, 52, 'Số 116', '01669574322', 'ngovanquy17091997@gmail.com', b'1'),
(5, 'Đức', 10, 45, 'Số 77', '0166957453', 'TranVanNam2008@gmail.com', b'0');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `groupproduct`
--

CREATE TABLE `groupproduct` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `groupproduct`
--

INSERT INTO `groupproduct` (`id`, `name`) VALUES
(1, 'Đồ Nam'),
(2, 'Đồ Nữ'),
(3, 'Áo Khoác'),
(4, 'Giày Nam'),
(5, 'Giày Nữ'),
(6, 'Nón'),
(7, 'Đồng Hồ');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `image` varchar(30) NOT NULL,
  `noibat` bit(1) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `images`
--

INSERT INTO `images` (`id`, `image`, `noibat`, `product_id`) VALUES
(1, 'ao_thun_nam_1.jpg', b'1', 5),
(2, 'ao_thun_nam_2.jpg', b'1', 6),
(3, 'ao_thun_nam_3.jpg', b'1', 7),
(4, 'ao_thun_nam_4.jpg', b'1', 8),
(5, 'ao_thun_nam_5.jpg', b'1', 9),
(6, 'ao_thun_nam_6.jpg', b'1', 10),
(7, 'chan_vay_1.jpg', b'1', 11),
(8, 'chan_vay_2.jpg', b'1', 12),
(9, 'chan_vay_3.jpg', b'1', 13),
(10, 'chan_vay_4.jpg', b'1', 14),
(11, 'ao_thun_nam_5_2.jpg', b'0', 5),
(12, 'ao_thun_nam_5_3.jpg', b'0', 5);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `custormers_id` int(11) NOT NULL,
  `total_money` double NOT NULL,
  `date` date NOT NULL,
  `status_order` bit(1) NOT NULL,
  `status_pay` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `orders`
--

INSERT INTO `orders` (`id`, `custormers_id`, `total_money`, `date`, `status_order`, `status_pay`) VALUES
(17, 5, 3370000, '2018-08-07', b'1', b'1'),
(18, 4, 1440000, '2018-08-07', b'0', b'0'),
(19, 4, 200000, '2018-08-07', b'0', b'0');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order_detail`
--

CREATE TABLE `order_detail` (
  `id` int(11) NOT NULL,
  `orders_id` int(11) NOT NULL,
  `product_detail_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `order_detail`
--

INSERT INTO `order_detail` (`id`, `orders_id`, `product_detail_id`, `quantity`) VALUES
(25, 17, 4, 2),
(26, 17, 1, 3),
(27, 17, 7, 2),
(28, 18, 7, 3),
(29, 19, 9, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `detail` varchar(500) NOT NULL,
  `price` double NOT NULL,
  `pricenews` double NOT NULL,
  `chatlieu` varchar(100) NOT NULL,
  `xuatxu` varchar(255) NOT NULL,
  `noibat` bit(1) NOT NULL,
  `typeproduct_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `product`
--

INSERT INTO `product` (`id`, `name`, `detail`, `price`, `pricenews`, `chatlieu`, `xuatxu`, `noibat`, `typeproduct_id`) VALUES
(5, 'Áo Thun Nam 0001', 'Thoải Mái Mát Mẻ Co Dãn', 500000, 490000, 'Cotton', 'Quảng Châu', b'1', 1),
(6, 'Áo Thun Nam 0002', 'Co Dãn Thoải Mái Mát Mẻ', 500000, 470000, 'Cotton', 'Hà Lội', b'1', 1),
(7, 'Áo Thun Nam 0003', 'Thiết Kế Tinh Tế Mang lại Sự Thoải Mái', 500000, 480000, 'Vải Thun Co Dãn', 'Hà Nam', b'1', 1),
(8, 'Áo Thun Nam 0004', 'Thiết Kế Tinh Tế Mang lại Sự Thoải Mái', 500000, 480000, 'Vải Thun Co Dãn', 'Hà Nam', b'0', 1),
(9, 'Áo Thun Nam 0005', 'Thiết Kế Tinh Tế Mang lại Sự Thoải Mái', 500000, 480000, 'Vải Thun Co Dãn', 'Hà Đức', b'1', 1),
(10, 'Áo Thun Nam 0006', 'Thiết Kế Tinh Tế Mang lại Sự Thoải Mái', 500000, 480000, 'Vải Thun Co Dãn', 'Nam Định', b'0', 1),
(11, 'Chân Váy 0001', 'Mềm Mại Thoáng Mát :D', 300000, 200000, 'Cotton', 'Quảng Châu', b'1', 8),
(12, 'Chân Váy 0002', 'Thiết Kế Tinh Tế', 300000, 270000, 'Vải Mềm', 'Hà Lội', b'1', 8),
(13, 'Chân Váy 0003', 'Thiết Kế Tinh Tế', 320000, 300000, 'Vải Mềm', 'Hà Lội', b'0', 8),
(14, 'Chân Váy 0004', 'Thiết Kế Tinh Tế', 320000, 300000, 'Vải Mềm', 'Hà Lội', b'0', 8);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_detail`
--

CREATE TABLE `product_detail` (
  `id` int(11) NOT NULL,
  `color` varchar(20) NOT NULL,
  `size` varchar(20) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `product_detail`
--

INSERT INTO `product_detail` (`id`, `color`, `size`, `product_id`, `quantity`) VALUES
(1, 'Trắng', 'XL', 5, 10),
(2, 'Đen', 'XXL', 5, 50),
(3, 'Vàng', 'XL', 5, 190),
(4, 'Trắng', 'XL', 6, 10),
(5, 'Trắng', 'XL', 7, 10),
(6, 'Trắng', 'XL', 10, 10),
(7, 'Trắng', 'XL', 8, 10),
(8, 'Trắng', 'XL', 9, 12),
(9, 'Trắng', 'L', 11, 0),
(10, 'Trắng', 'L', 11, 11),
(11, 'Vàng', 'XL', 12, 12),
(12, 'Vàng', 'L', 13, 1),
(13, 'Vàng', 'M', 14, 4),
(14, 'Đen', 'M', 14, 12),
(15, 'Trắng', 'XXL', 5, 11);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `slide`
--

CREATE TABLE `slide` (
  `id` int(11) NOT NULL,
  `TieuDe` varchar(20) NOT NULL,
  `TomTat` varchar(30) NOT NULL,
  `image` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `slide`
--

INSERT INTO `slide` (`id`, `TieuDe`, `TomTat`, `image`) VALUES
(1, 'Event 2018', 'Summer Hot !', 'image_slide_event_2018.jpg');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `typeproduct`
--

CREATE TABLE `typeproduct` (
  `id` int(11) NOT NULL,
  `name` varchar(40) NOT NULL,
  `groupproduct_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `typeproduct`
--

INSERT INTO `typeproduct` (`id`, `name`, `groupproduct_id`) VALUES
(1, 'Áo Thun', 1),
(2, 'Quần Short', 1),
(3, 'Áo Sơ Mi', 1),
(4, 'Quần Jean', 1),
(5, 'Áo Thun', 2),
(6, 'Quần Jean', 2),
(7, 'Đầm Nữ', 2),
(8, 'Chân Váy', 2);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL,
  `custormers_id` int(11) NOT NULL,
  `quyen` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `custormers_id`, `quyen`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 4, b'1');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `address_detail`
--
ALTER TABLE `address_detail`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `address_district`
--
ALTER TABLE `address_district`
  ADD PRIMARY KEY (`id`),
  ADD KEY `address_province_id` (`address_province_id`);

--
-- Chỉ mục cho bảng `address_province`
--
ALTER TABLE `address_province`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_address_detail` (`address_detail`),
  ADD KEY `id_address_district` (`id_address_district`),
  ADD KEY `id_address_province` (`id_address_province`);

--
-- Chỉ mục cho bảng `groupproduct`
--
ALTER TABLE `groupproduct`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Chỉ mục cho bảng `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `custormers_id` (`custormers_id`);

--
-- Chỉ mục cho bảng `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_id` (`orders_id`),
  ADD KEY `product_id` (`product_detail_id`);

--
-- Chỉ mục cho bảng `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `typeproduct_id` (`typeproduct_id`);

--
-- Chỉ mục cho bảng `product_detail`
--
ALTER TABLE `product_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Chỉ mục cho bảng `slide`
--
ALTER TABLE `slide`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `typeproduct`
--
ALTER TABLE `typeproduct`
  ADD PRIMARY KEY (`id`),
  ADD KEY `groupproduct_id` (`groupproduct_id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `custormers_id` (`custormers_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `address_detail`
--
ALTER TABLE `address_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `address_district`
--
ALTER TABLE `address_district`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT cho bảng `address_province`
--
ALTER TABLE `address_province`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `groupproduct`
--
ALTER TABLE `groupproduct`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT cho bảng `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT cho bảng `order_detail`
--
ALTER TABLE `order_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT cho bảng `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT cho bảng `product_detail`
--
ALTER TABLE `product_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT cho bảng `slide`
--
ALTER TABLE `slide`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `typeproduct`
--
ALTER TABLE `typeproduct`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `address_district`
--
ALTER TABLE `address_district`
  ADD CONSTRAINT `fk_distic_province` FOREIGN KEY (`address_province_id`) REFERENCES `address_province` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `fk_district` FOREIGN KEY (`id_address_district`) REFERENCES `address_district` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_province` FOREIGN KEY (`id_address_province`) REFERENCES `address_province` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `fk_image` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `fk_oders_custormer` FOREIGN KEY (`custormers_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `order_detail`
--
ALTER TABLE `order_detail`
  ADD CONSTRAINT `fk_orders` FOREIGN KEY (`orders_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_orders_product` FOREIGN KEY (`product_detail_id`) REFERENCES `product_detail` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `fk_typeproduct` FOREIGN KEY (`typeproduct_id`) REFERENCES `typeproduct` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `product_detail`
--
ALTER TABLE `product_detail`
  ADD CONSTRAINT `fk_product_detail` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `typeproduct`
--
ALTER TABLE `typeproduct`
  ADD CONSTRAINT `fk_grouproduct` FOREIGN KEY (`groupproduct_id`) REFERENCES `groupproduct` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_custormer_user` FOREIGN KEY (`custormers_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
