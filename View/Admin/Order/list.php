 <!-- Page Content -->
 <?php 
    $list = $data['list'];
 ?>
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Category
                            <small>List</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>Order ID</th>
                                <th>Customer Id</th>
                                <th>TotalMoney</th>
                                <th>Date</th>
                                <th>Status order</th>
                                <th>Status Pay</th>
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                foreach ($list as $value) {
                                ?>
                                     <tr class="odd gradeX" align="center">
                                        <td><?=$value->id?></td>
                                        <td><?=$value->custormers_id?></td>
                                        <td><?=number_format($value->total_money)?></td>
                                        <td><?=$value->date?></td>
                                        <td><?php if($value->status_order == 1){echo "đã chuyển";}else{ echo 'chưa chuyển';}  ?></td>
                                        <td><?php if($value->status_order == 1){echo 'đã thanh toán';}else{echo 'chưa thanh toán';} ?></td>
                                        <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="index.php?c=admin&c2=Order&a=delete&id=<?=$value->id?>"> Delete</a></td>
                                        <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="index.php?c=admin&c2=Order&a=update&id=<?=$value->id?>">Detail</a></td>
                                    </tr>

                                    <?php
                                }
                             ?>
                   
                        </tbody>
                    </table>
                    <?php
                        if(isset($_SESSION['delete_order']))
                        {
                            if(time() - $_SESSION['time_delete_order'] > 20)
                            {
                                unset($_SESSION['delete_order']);
                            }
                            echo '<div  align="center" class="alert alert-success">'.$_SESSION['delete_order'].'</div>'; 
                        }
                         if(isset($_SESSION['success_update']))
                        {
                            if(time() - $_SESSION['success_update_time'] > 20)
                            {
                                unset($_SESSION['success_update']);
                            }
                            echo '<div  align="center" class="alert alert-success">'.$_SESSION['success_update'].'</div>';
                        }
                        if(isset($_SESSION['delete_groupproduct']))
                        {
                            if(time()-$_SESSION['delete_groupproduct_time']>20)
                            {
                                unset($_SESSION['delete_groupproduct']);
                            }
                            echo '<div align="center" class="alert alert-success">'.$_SESSION['delete_groupproduct'].'</div>';
                        }
                    ?>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->