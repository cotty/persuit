<?php
    $order_detail = $data['order_detail'];
?>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Order Detail
                    <small>List Product Order</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                    <tr align="center">
                        <th>Order_detail ID</th>
                        <th>Order Quantity</th>
                        <th>Product_detail Id</th>
                        <th>Product Size</th>
                        <th>product Color</th>
                        <th>product Image</th>
                        <th>product Id</th>
                        <th>product Name</th>
                        <th>product Price</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                        $sp = explode(',',$order_detail->SP);
                        foreach ($sp as $value) {
                        list($odd_id,$odd_quantity,$pdd_id,$pd_size,$pd_color,$pd_id,$pd_name,$pd_price,$pd_image) = explode('/',$value);
                ?>
                    <tr class="odd gradeX" align="center">
                        <td><?=$odd_id?></td>
                        <td><?=$odd_quantity?></td>
                        <td><?=$pdd_id?></td>
                        <td><?=$pd_size?></td>
                        <td><?=$pd_color?></td>
                        <td><?=$pd_image?></td>
                        <td><?=$pd_id?></td>
                        <td><?=$pd_name?></td>
                        <td><?=$pd_price?></td>
                    </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <div id="page-wrapper">
             <div class="col-lg-12">
                        <h1 class="page-header">Change Status
                        </h1>
                    </div>
             <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="" method="POST">
                            <div class="form-group">
                                <label> Status Orders</label>
                                <label class="radio-inline">
                                    <input name="rdo_Order" value="0" <?php if($order_detail->status_order == 0){echo 'checked';}?> type="radio">Chưa Chuyển Hàng
                                </label>
                                <label class="radio-inline">
                                    <input name="rdo_Order" value="1" <?php if($order_detail->status_order == 1){echo 'checked';}?> type="radio">Đã Chuyển
                                </label>
                            </div>
                            <div class="form-group">
                                <label> Status Pay</label>
                                <label class="radio-inline">
                                    <input name="rdo_pay" value="0" <?php if($order_detail->status_pay == 0){echo 'checked';}?> type="radio">Chưa Thanh Toán
                                </label>
                                <label class="radio-inline">
                                    <input name="rdo_pay" value="1" <?php if($order_detail->status_pay == 1){echo 'checked';}?> type="radio">Đã Thanh Toán
                                </label>
                            </div>
                            <button type="submit" name="update" class="btn btn-default">Update</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        <form>
                    </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>