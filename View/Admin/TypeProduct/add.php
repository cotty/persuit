 <?php 
    $listgroup = $data['groupproduct'];
?>
    <div id="wrapper">
        <!-- Navigation -->
       
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Type Product
                            <small>Add</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="" method="POST">
                            <div class="form-group">
                                <label>Type Product Name</label>
                                <input class="form-control" name="Name" placeholder="Please Enter Type Product" />
                            </div>
                            <div class="form-group">
                                <label>Group Product Parent</label>
                                <select class="form-control" name ="groupProductId">
                                    <option value="0">Please Choose Group Product</option>
                                    <?php 
                                        foreach ($listgroup as $value) {
                                            ?>
                                            <option value="<?=$value->id?>"><?=$value->name?></option>
                                            <?php
                                        }
                                     ?>
                                </select>
                            </div>
                            <button type="submit" name="Add" class="btn btn-default">Add</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                            <?php if (isset($_SESSION['error_add']))
                            {
                            echo '<p align="center" class="alert alert-danger">'.$_SESSION['error_add'].'</p>'; 
                                 if(time() - $_SESSION['error_add_time'] > 20)
                                {
                                    unset($_SESSION['error_add']);
                                }
                            }
                            ?>
                            
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
