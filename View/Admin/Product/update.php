 <?php 
 $product= $data['product'];
 $random = rand(1,1000000000);
 $_SESSION['token'] =  $random;
 $pdd_detail = $data['product_detail'];
 $image = $data['image'];
 $listType = $data['listType'];

?>
    <div id="wrapper">
        <!-- Navigation -->
       
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Product
                            <small>Add</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="" method="POST" enctype="multipart/form-data">
                             <input class="form-control" type="hidden" name="token" id="token" value="<?=$random?>" />
                             <div class="form-group">
                                <label>Product Id</label>
                                <input class="form-control" name="id" value="<?=$product->id?>" disabled placeholder="Please Enter .." />
                            </div>
                            <div class="form-group">
                                <label>Product Name</label>
                                <input class="form-control" name="name" value="<?=$product->name?>" placeholder="Please Enter .." />
                            </div>
                            <div class="form-group">
                                <label>Product Detail</label>
                                <input class="form-control" name="detail"  value="<?=$product->detail?>" placeholder="Please Enter..." />
                            </div>
                            <div class="form-group">
                                <label>Product Price</label>
                                <input class="form-control" name="price" value="<?=$product->price?>" placeholder="Please Enter ..." />
                            </div>
                            <div class="form-group">
                                <label>Product Price News</label>
                                <input class="form-control" name="pricenews" value="<?=$product->pricenews?>" placeholder="Please Enter ..." />
                            </div>
                            <div class="form-group">
                                <label>Chất Liệu</label>
                                <input class="form-control" name="chatlieu"  value="<?=$product->chatlieu?>" placeholder="Please Enter..." />
                            </div>
                            <div class="form-group">
                                <label>Xuất Xứ</label>
                                <input class="form-control" name="xuatxu" value="<?=$product->xuatxu?>" placeholder="Please Enter..." />
                            </div>
                            <div class="form-group">
                                <label>Nổi Bật</label>
                                <label class="radio-inline">
                                    <input name="noibat" value="1" <?php if ($product->noibat == 1) echo 'checked';?> type="radio">Có
                                </label>
                                <label class="radio-inline">
                                    <input name="noibat" value="0" <?php if ($product->noibat == 0) echo 'checked';?> type="radio">Không
                                </label>
                            </div>
                             <div class="form-group">
                                <label>Type Product Parent</label>
                                <select class="form-control" name ="typeproduct_id">
                                    <option value="0">Please Choose Type Product</option>
                                    <?php 
                                        foreach ($listType as $value) {
                                            ?>
                                            <option <?php if($value->id == $product->typeproduct_id){echo 'selected';} ?> value="<?=$value->id?>"><?=$value->name?></option>
                                            <?php
                                        }
                                     ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Quantity Product Detail</label>
                                <select class="form-control product_detail" name ="product_detail" id="product_detail">
                                    <option value="0">Please Choose Quantity Type Product Detail</option>
                                    <?php 
                                        for($i= 1;$i<7;$i++)
                                        {
                                            ?>
                                            <option <?php if($i==count($pdd_detail)) { echo 'selected';} ?> value="<?=$i?>"><?=$i?></option>
                                            <?php
                                        }
                                     ?>
                                </select>
                            </div>
                            <?php 
                            $count1 =0;
                                foreach ($pdd_detail as $pdd) {
                                    $count1++;
                                    ?>
                                      <div id="productdetail<?=$count1?>" style="display: none;">
                                        <label>Product Detail <?=$count1?></label>
                                         <div class="form-group" style="margin-left:30px;">
                                            <label>Product Detail Id</label>
                                            <input class="form-control" value="<?=$pdd->id?>" name="idpdd<?=$count1?>" />
                                            <label>Product Color</label>
                                            <input class="form-control" value="<?=$pdd->color?>" name="color<?=$count1?>" placeholder="Please Enter ..." />
                                            <label>Product Size</label>
                                            <input class="form-control" value="<?=$pdd->size?>" name="size<?=$count1?>" placeholder="Please Enter ..." />
                                            <label>Product Quantity</label>
                                            <input class="form-control" value="<?=$pdd->quantity?>" name="quantity<?=$count1?>" placeholder="Please Enter ..." />
                                        </div>
                                    </div>
                                    <?php
                                }

                                for ($i = 6;$i>=$count1;$i--)
                                {
                                    ?>
                                     <div id="productdetail<?=$i?>" style="display: none;">
                                        <label>Product Detail <?=$i?></label>
                                         <div class="form-group" style="margin-left:30px;">
                                            
                                            <input class="form-control" name="color<?=$i?>" placeholder="Please Enter ..." />
                                            <label>Product Size</label>
                                            <input class="form-control" name="size<?=$i?>" placeholder="Please Enter ..." />
                                            <label>Product Quantity</label>
                                            <input class="form-control" name="quantity<?=$i?>" placeholder="Please Enter ..." />
                                        </div>
                                    </div>
                                    <?php
                                }
                            ?>
                            <script type="text/javascript">
                                   $(document).ready(function(){
                                    var sl = $("#product_detail").val();
                                    for(var i =0; i <= sl;i++)
                                        {
                                            $('#productdetail'+i).css("display","block");
                                        }
                                    $("#product_detail").change(function(){
                                        var sl = $("#product_detail").val();
                                        for(var i =0; i <= sl;i++)
                                        {
                                            $('#productdetail'+i).css("display","block");
                                        }
                                        for(var j = 10;j>sl;j--)
                                        {
                                             $('#productdetail'+j).css("display","none");
                                        }
                                    });
                                     var sl2 = $("#product_image").val();
                                        for(var i =0; i <= sl2;i++)
                                        {
                                            $('#productimage'+i).css("display","block");
                                        }
                                     $("#product_image").change(function(){
                                        var sl2 = $("#product_image").val();
                                        for(var i =0; i <= sl2;i++)
                                        {
                                            $('#productimage'+i).css("display","block");
                                        }
                                         for(var j = 10;j>sl2;j--)
                                        {
                                             $('#productimage'+j).css("display","none");
                                        }
                                    });
                                   });  
                            </script>
                            <div class="form-group">
                                <label>Quantity Product Image</label>
                                <select class="form-control product_image" name ="product_image" id="product_image">
                                    <option value="0">Please Choose Quantity Image Product</option>
                                    <?php 
                                        for($i= 1;$i<5;$i++)
                                        {
                                            ?>
                                            <option <?php if($i == count($image)){echo 'selected';} ?> value="<?=$i?>"><?=$i?></option>
                                            <?php
                                        }
                                     ?>
                                </select>
                            </div>
                            <?php
                            $count =0;
                                foreach ($image as $img) {
                                    $count++;
                                    ?>
                                        <div id="productimage<?=$count?>" style="display: none;">
                                            <label>Product Image <?=$count?></label>
                                             <div class="form-group" style="margin-left:30px;">
                                                <label>Product Detail Id</label>
                                            <input class="form-control" value="<?=$img->id?>" name="idImage<?=$count?>"/> 
                                                <img src="public/img/product/<?=$img->image?>" width="200px" height="200px" alt=""/><br/>
                                                <label>Choice Iamge</label>
                                                <input  type="file" name="image<?=$count?>" value="asdsad" placeholder="Please Enter ..." />
                                            </div>
                                                <div class="form-group">
                                                <label>Nổi Bật</label>
                                                <label class="radio-inline">
                                                    <input name="noibatImage<?=$count?>" value="1" <?php if($img->noibat == 1){echo 'checked';}?> type="radio">Có
                                                </label>
                                                <label class="radio-inline">
                                                    <input name="noibatImage<?=$count?>" value="0" <?php if ($img->noibat == 0) echo 'checked';?>  type="radio">Không
                                                </label>
                                            </div>
                                        </div>
                                    <?php
                                }

                                for($j = 4; $j>=$count;$j--)
                                {
                                    ?>
                                    <div id="productimage<?=$j?>" style="display: none;">
                                        <label>Product Image <?=$j?></label>
                                         <div class="form-group" style="margin-left:30px;">
                                            <label>Choice Iamge</label>
                                            <input  type="file" name="image<?=$j?>" placeholder="Please Enter ..." />
                                        </div>
                                            <div class="form-group">
                                            <label>Nổi Bật</label>
                                            <label class="radio-inline">
                                                <input name="noibatImage<?=$j?>" value="1" checked="" type="radio">Có
                                            </label>
                                            <label class="radio-inline">
                                                <input name="noibatImage<?=$j?>" value="0" type="radio">Không
                                            </label>
                                        </div>
                                    </div>
                                    <?php
                                }
                            ?>
                            <button type="submit" name="update" class="btn btn-default">Update</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                            <?php if (isset($_SESSION['error_add']))
                            {
                            echo '<p align="center" class="alert alert-danger">'.$_SESSION['error_add'].'</p>'; 
                                 if(time() - $_SESSION['error_add_time'] > 20)
                                {
                                    unset($_SESSION['error_add']);
                                }
                            }
                            ?>
                            
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
