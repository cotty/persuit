
<footer class="footer_area">
            <div class="container">
                <div class="footer_widgets">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-6">
                            <aside class="f_widget f_about_widget">
                                <img src="public/img/logo.png" alt="">
                                <p>Persuit is a Premium PSD Template. Best choice for your online store. Let purchase it to enjoy now</p>
                                <h6>Social:</h6>
                                <ul>
                                    <li><a href="#"><i class="social_facebook"></i></a></li>
                                    <li><a href="#"><i class="social_twitter"></i></a></li>
                                    <li><a href="#"><i class="social_pinterest"></i></a></li>
                                    <li><a href="#"><i class="social_instagram"></i></a></li>
                                    <li><a href="#"><i class="social_youtube"></i></a></li>
                                </ul>
                            </aside>
                        </div>
                                    <?php
                                        foreach ($data['menu'] as $mn) {
                                            ?>
                                               <div class="col-lg-2 col-md-2 col-6">
                                                <aside class="f_widget link_widget f_info_widget">
                                                    <div class="f_w_title">
                                                        <h3><?=$mn->name?></h3>
                                                    </div>
                                                    <ul>
                                            <?php
                                            $list1 = explode(',', $mn->tpp);
                                            foreach ($list1 as $value) {
                                                 list($id,$name) = explode('/', $value);
                                                 ?>
                                                     <li><a href="?c=HomePage&a=category&id=<?=$id?>"><?=$name?></a></li>
                                                 <?php
                                            }
                                           
                                            ?>
                                             
                                                    </ul>
                                                </aside>
                                            </div>
                                            <?php

                                        }
                                    ?>
                                  
                          
                        
                    </div>
                </div>
                <div class="footer_copyright">
                    <h5>© <script>document.write(new Date().getFullYear());</script> <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved </a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
</h5>
                </div>
            </div>
        </footer>
         <script src="./public/js/jquery-3.2.1.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="./public/js/popper.min.js"></script>
        <script src="./public/js/bootstrap.min.js"></script>
        <!-- Rev slider js -->
        <script src="./public/vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script src="./public/vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <script src="./public/vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script src="./public/vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <script src="./public/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script src="./public/vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="./public/vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script src="./public/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <!-- Extra plugin css -->
        <script src="./public/vendors/counterup/jquery.waypoints.min.js"></script>
        <script src="./public/vendors/counterup/jquery.counterup.min.js"></script>
        <script src="./public/vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="./public/vendors/bootstrap-selector/js/bootstrap-select.min.js"></script>
        <script src="./public/vendors/image-dropdown/jquery.dd.min.js"></script>
        <script src="./public/js/smoothscroll.js"></script>
        <script src="./public/vendors/isotope/imagesloaded.pkgd.min.js"></script>
        <script src="./public/vendors/isotope/isotope.pkgd.min.js"></script>
        <script src="./public/vendors/magnify-popup/jquery.magnific-popup.min.js"></script>
        <script src="./public/vendors/vertical-slider/js/jQuery.verticalCarousel.js"></script>
        <script src="./public/vendors/jquery-ui/jquery-ui.js"></script>
        
        <script src="./public/js/theme.js"></script>
    </body>
</html>