<?php
    $product = $data['product'];
    $product_detail = explode(',',$product->pd_detail);
    ?>
        
        <!--================Product Details Area =================-->
        <section class="product_details_area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5">
                        <div class="product_details_slider">
                            <div id="product_slider2" class="rev_slider" data-version="5.3.1.6">
                                <ul>	<!-- SLIDE  -->
                                    <?php
                                        $image = $data['images'];
                                        foreach ($image as $img) {
                                            ?>
                                            <li data-index="rs-28" data-transition="scaledownfromleft" data-slotamount="default"  data-easein="default" data-easeout="default" data-masterspeed="1500"  data-thumb="public/img/product/<?=$img->image?>"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Umbrella" data-param1="September 7, 2015" data-param2="Alfon Much, The Precious Stones" data-description="">
                                                <!-- MAIN IMAGE -->
                                                <img src="public/img/product/<?=$img->image?>"  alt=""  width="1920" height="1080" data-lazyload="public/img/product/<?=$img->image?>" data-bgposition="center center" data-bgfit="contain" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                                                <!-- LAYERS -->
                                            </li>
                                            <?php
                                        }
                                    ?>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="product_details_text">
                            <h3><?=$product->name?></h3>
                            <h4><?=number_format($product->pricenews)?> VNĐ</h4>
                            <p><?=$product->detail?></p>
                            <div class="p_color">
                                <h4 class="p_d_title">color <span>*</span></h4>
                                <select class="form-control custom-select getcolor" id="getColor">
                                    <option value="0">Color</option> 
                                   <?php
                                        $color = $data['color'];
                                        print_r($color);
                                        foreach ($color as $cl) {
                                            ?>
                                                <option value="<?=$cl->color?>"><?=$cl->color?></option> 
                                            <?php
                                        }
                                   ?>
                                    
                                </select>
                            </div>
        
                            <div class="p_color">
                                <h4 class="p_d_title">size <span>*</span></h4>
                                <select id="size" name="" style="width:100%;" class="custom-select">
                                     <option value="0">Size</option> 
                                </select>
                            
                            </div>
                             <script type="text/javascript">
                                $(document).ready(function(){
                                    $("#getColor").change(function(){
                                        var color = $("#getColor").val();
                                        var id = <?=json_encode($product->id)?>;
                                        $.post("Ajax/getSizeByColor.php",{id:id,color:color},function(data){
                                          $("#size").html(data);
                                        });
                                    });
                                    $("#size").change(function(){
                                        
                                        $(".soluong").css("display","block");
                                        var color = $("#getColor").val();
                                        var size =   $("#size").val();
                                         var id = <?=json_encode($product->id)?>;
                                         $.ajax({
                                            url:"Ajax/getsoluong.php",
                                            method:"POST",
                                            data: "id="+id+"&color="+color+"&size="+size,
                                            dataType:"json",
                                            success:function(data)
                                            {
                                                console.log(data);
                                                $("#sl").text(data.quantity);
                                                $("#sl").attr("class",data.id);
                                            }
                                         });
                                        
                                    });
                                    $(".add_cart_btn").click(function(){
                                        if($("#getColor").val() == "0" || $("#size").val()==0)
                                        {
                                            alert('Vui lòng chọn color And size !');
                                        }else{
                                            var id = document.getElementById("sl").getAttribute("class");
                                            var quantity = document.getElementById("sst").value;
                                            $.get("index.php?c=ShoppingCart&a=init",{id:id,quantity:quantity},function(data){
                                                $(".custom").append(data);
                                            });
                                        }
                                    });
                                });
                            </script>
                            <div style="display:none;margin-top:15px;" class="soluong">Số Lượng Có : <span id="sl"></span></div> 
                            <div class="quantity">
                                <div class="custom">
                                    <button onclick="var result = document.getElementById('sst'); var sst = result.value; if( !isNaN( sst ) &amp;&amp; sst > 0 ) result.value--;return false;" class="reduced items-count" type="button"><i class="icon_minus-06"></i></button>
                                    <input type="text" name="qty" id="sst" maxlength="12" value="01" title="Quantity:" class="input-text qty">
                                    <button onclick="var result = document.getElementById('sst');
                                   var sst = result.value; var result4=parseInt(document.getElementById('sl').textContent);
                                         console.log(result4);
                                     if( !isNaN( sst) && sst<result4) result.value++;return false;" class="increase items-count" type="button"><i class="icon_plus"></i></button>
                                </div>
                                 <button class="add_cart_btn">ADD TO CARD</button>
                            </div>
                            <div class="shareing_icon">
                                <h5>share :</h5>
                                <ul>
                                    <li><a href="#"><i class="social_facebook"></i></a></li>
                                    <li><a href="#"><i class="social_twitter"></i></a></li>
                                    <li><a href="#"><i class="social_pinterest"></i></a></li>
                                    <li><a href="#"><i class="social_instagram"></i></a></li>
                                    <li><a href="#"><i class="social_youtube"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--================End Product Details Area =================-->
        
        <!--================End Related Product Area =================-->
        
        <!--================End Related Product Area =================-->
        
        <!--================Footer Area =================-->
    