<!--================login Area =================-->
        <section class="login_area p_100">
            <div class="container">
                <div class="login_inner">
                    <div class="row">
                        <div class="col-lg-4" style="text-align: center; margin-left:30%;">
                            <div class="login_title">
                                <h2>log in your account</h2>
                            </div>
                            <form class="login_form row" method="POST">
                                <div class="col-lg-12 form-group">
                                    <input class="form-control" type="text" name="userName" placeholder="Name">
                                </div>
                                <div class="col-lg-12 form-group">
                                    <input class="form-control" type="password" name="password" placeholder="User Name">
                                </div>
                                <div class="col-lg-12 form-group">
                                    <h4>Forgot your password ?</h4>
                                </div>
                                <div class="col-lg-12 form-group">
                                    <button type="submit" value="submit" class="btn update_btn form-control" name="btn-login">Login</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--================End login Area =================-->
        