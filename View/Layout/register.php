 <?php
    $list_province = $data['allprovince'];
    $random = rand(1,100000);
    $_SESSION['token'] = $random;
 ?>
 <section class="login_area p_100" style="margin-left:30%;">
    <div class="container">
        <div class="login_inner">
            <div class="row">
                <div class="col-lg-8">
                    <div class="login_title">
                        <h2>create account</h2>
                        <p>Follow the steps below to create email account enjoy the great mail.com emailing experience. Vivamus tempus risus vel felis condimentum, non vehicula est iaculis.</p>
                    </div>
                    <form class="login_form row" method="POST">
                        <input class="form-control" type="hidden" value="<?=$random?>" name="token" placeholder="Name">
                        <div class="col-lg-6 form-group">
                            <label for="">Name</label>
                            <input class="form-control" type="text" name="name" placeholder="Name">
                        </div>
                        <div class="col-lg-6 form-group">
                             <label for="">Email</label>
                            <input class="form-control" type="email" name="email" placeholder="Email">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">Province</label>
                            <select class="form-control custom-select" name="province" id="province">
                                <option selected>Choose Province ...</option>
                                <?php
                                    foreach ($list_province as $value) {
                                        ?>
                                        <option value="<?=$value->id?>"><?=$value->name_province?></option>
                                        <?php
                                    }
                                ?>
                              </select>
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">District</label>
                            <select class="form-control custom-select" name="district" id="district">
                                <option selected>Choose District ...</option>
                              </select>
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for=""> Place Detail</label>
                            <input class="form-control" Name="place_detail" type="text" name="place_detail" placeholder="Place detail...">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">Phone Number</label>
                            <input class="form-control" Name="tel" type="text" name="tel" placeholder="phone number...">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">User Name</label>
                            <input class="form-control" type="text" name="username" placeholder="User Name">
                        </div>
                        <div class="col-lg-6 form-group">
                            <label for="">Password</label>
                            <input class="form-control" type="password" name="password" placeholder="Password">
                        </div>
                        <div class="col-lg-6 form-group">
                            <button type="submit" style="margin-left:40%;" name ="register" value="submit" class="btn subs_btn form-control">register now</button>
                        </div>
                        <?php 
                            if(isset($_SESSION['error_register']))
                            {
                                echo '<div class="alert alert-danger">'.$_SESSION['error_register'].'</div>';
                                if(time() -  $_SESSION['error_register_time'] > 60)
                                {
                                    unset($_SESSION['error_register']);
                                }
                            }
                            ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function(){
        $("#province").change(function(){
            var id = $("#province").val();  
            $.get("Ajax/getDistrict.php",{id:id},function(data){
                $("#district").html(data);
            });
        });
    });
</script>