<?php
    $typeproduct_byid = $data['typeproduct_byid'];
    $product = $data['product_bytype'];
    $count = count($product);
    $page = $count/3;
    $product_phantrang = $data['product_phantrang'];
?>
<div class="category" >
    <div class="container header" style="background: lightgray;padding: 20px 0px;text-align:center;margin-top:5px;">
        <h4 style="padding-left:50px;"><?=$typeproduct_byid->name?></h4>
    </div>
</div>
<section class="no_sidebar_2column_area">
    <div class="container">
        <div class="two_column_product">
            <div class="row">
                <?php
                    foreach ($product_phantrang as $pdpt) {
                        list($id,$name) = explode('/',$pdpt->img);
                       ?>
                        <div class="col-lg-4 col-sm-6">
                            <div class="l_product_item">
                                <div class="l_p_img">
                                    <img class="img-fluid" src="public/img/product/<?=$name?>" alt="">
                                </div>
                                <div class="l_p_text">
                                   <ul>
                                        <li class="p_icon"><a href="#"><i class="icon_piechart"></i></a></li>
                                        <li><a class="add_cart_btn" href="?c=HomePage&a=detail&id=<?=$pdpt->id?>">Chi Tiết</a></li>
                                        <li class="p_icon"><a href="#"><i class="icon_heart_alt"></i></a></li>
                                    </ul>
                                    <h4><?=$pdpt->name?></h4>
                                    <h5><del style="padding-right: 10px;"><?=number_format($pdpt->price)?> VNĐ</del><?=number_format($pdpt->pricenews)?> VNĐ</h5>
                                </div>
                            </div>
                        </div>
                       <?php 
                    }
                ?>
                        
                
            </div>
            <nav aria-label="Page navigation example" class="pagination_area">
              <ul class="pagination">
                <?php
                $vitri = isset($_GET['page'])?$_GET['page']:1;
                if($vitri ==1)
                {
                    for($i =1;$i<$page+1;$i++)
                    {
                        ?>
                        <li class="page-item"><a class="page-link"  href="?c=HomePage&a=category&id=<?=$typeproduct_byid->id?>&page=<?=$i?>"><?=$i?></a></li>
                        <?php
                    }
                ?>
                <li class="page-item next"><a class="page-link" onclick="nextpage()" href="?c=HomePage&a=category&id=<?=$typeproduct_byid->id?>&page=<?=$vitri+1?>"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                <?php
                }else{
                    ?>
                     <li class="page-item next"><a class="page-link" onclick="nextpage()" href="?c=HomePage&a=category&id=<?=$typeproduct_byid->id?>&page=<?=$vitri-1?>"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
                    <?php
                     for($i =1;$i<$page+1;$i++)
                    {
                        ?>
                        
                        <li class="page-item"><a class="page-link" <?php if($i == $vitri) {echo 'style="border:1px solid black;"';} ?> href="?c=HomePage&a=category&id=<?=$typeproduct_byid->id?>&page=<?=$i?>"><?=$i?></a></li>
                        <?php
                        }
                        $vitri++;
                    ?>
                    <li class="page-item next"><a class="page-link" onclick="nextpage()" href="?c=HomePage&a=category&id=<?=$typeproduct_byid->id?>&page=<?=$vitri?>"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                    <?php
                }
                ?>
              </ul>
            </nav>
            <script type="text/javascript">
                    
            </script>
        </div>
    </div>
</section>

       