 <header class="shop_header_area">
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <a class="navbar-brand" href="#"><img src="public/img/logo.png" alt=""></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    
                        <ul class="navbar-nav">
                               <li class="nav-item"><a class="nav-link" href="index.php">HOME</a></li>
                            <?php
                            $menu = $data['menu'];
                            foreach ($menu as $mn) {
                                $list = explode(',',$mn->tpp);
                                ?>
                                    <li class="nav-item dropdown submenu">
                                        <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <?=$mn->name?> <i class="fa fa-angle-down" aria-hidden="true"></i>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <?php
                                            foreach ($list as $lt) {
                                                list($tp_id,$tp_name) = explode('/',$lt);
                                            ?>
                                            <li class="nav-item"><a class="nav-link" href="?c=HomePage&a=category&id=<?=$tp_id?>"><?=$tp_name?></a></li>
                                            <?php

                                            }
                                            ?>
                                            
                                            
                                        </ul>
                                    </li>
                                <?php
                            }
                            ?>
                            
                            <li class="nav-item"><a class="nav-link" href="index.php?c=HomePage&a=OrderDetail">Check Order</a></li>
                            <li class="nav-item"><a class="nav-link" href="?c=HomePage&a=contact">Contact</a></li>
                            <?php if(isset($_SESSION['user'])){ if($_SESSION['user']->quyen == 1) { echo '<li class="nav-item"><a class="nav-link" href="index.php?c=admin">Quản Lý</a></li>'; } } ?>
                        </ul>
                    </div>
                </nav>
            </div>
        </header>