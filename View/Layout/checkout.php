<?php
    $listproduct = $data['listproduct'];
    $list_province  = $data['list_province'];
    $random = rand(1,1000000000);
	$_SESSION['token'] =  $random;
?>
<section class="shopping_cart_area p_100">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10">
                        <div class="cart_items" style="margin-left:10%;">
                            <h3>Your Cart Items</h3>
                            <div class="table-responsive-md" >
                                <table class="table">
                                    <tbody>
                                        <?php
                                            $totalMoney = 0;
                                            foreach ($listproduct as $value) {
                                                $totalMoney += ($value['item_pricenews']*$value['item_quantity']);
                                            ?>
                                            <tr>
                                                <th scope="row">
                                                    <a href="index.php?c=ShoppingCart&a=delete&id=<?=$value['item_id']?>" onclick="thongbao();"><img src="public/img/icon/close-icon.png" alt=""></a>
                                                </th>
                                                <td>
                                                    <div class="media">
                                                        <div class="d-flex">
                                                            <img src="public/img/product/<?=$value['item_image']?>" width="100px" height="150px" alt="">
                                                        </div>
                                                        <div class="media-body">
                                                            <h4><?=$value['item_name']?><h4>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td><p class="red"><?=number_format($value['item_pricenews'])?> VNĐ</p></td>
                                                <td>
                                                    <div class="quantity">
                                                        <h6>Quantity: <?=$value['item_quantity']?></h6>
                                                       
                                                    </div>
                                                </td>
                                                <td><p><?=number_format($value['item_pricenews']*$value['item_quantity'])?> VNĐ</p></td>
                                             </tr>
                                            <?php      
                                            }
                                        ?>
                                        <tr>
                                            <th scope="row">
                                            </th>
                                        </tr>
                                        <tr class="last" >
                                            <th scope="row">
                                                <img src="img/icon/cart-icon.png" alt="">
                                            </th>
                                            <td>
                                                <div class="media">
                                                    <div class="d-flex">
                                                        <h5>Total Money:</h5>
                                                    </div>
                                                    <div class="media-body" >
                                                        <h5><?=number_format($totalMoney)?> VNĐ</h5>
                                                    </div>
                                                </div>
                                            </td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                </div>
                  <section class="login_area p_100">
            <div class="container">
                <div class="login_inner">
                    <div class="row">
                        <div class="col-lg-10" style="margin-left:5%;">
                            <div class="login_title">
                                <h2>Check Out</h2>
                                <p>Please press information to order product</p>
                            </div>
                            <form class="login_form row" method="POST">
                            	<?php

                            		if(isset($_SESSION['user']))
                            		{
                               			?>
                            	
		                                    <input  class="form-control" type="hidden" name="token" value="<?=$random?>" placeholder="Name">
                            			 <div class="col-lg-6 form-group">
                            			 	<label for="">Name</label>
		                                    <input  class="form-control" type="text" name="name" value="<?=$_SESSION['user']->Name?>" placeholder="Name">
		                                </div>
		                                <div class="col-lg-6 form-group">
		                                	<label for="">Province</label>
		                                    <input class="form-control" type="text" name="province" value="<?=$_SESSION['user']->name_province?>" placeholder="Email">
		                                </div>
		                                <div class="col-lg-6 form-group">
		                                	<label for="">District</label>
		                                    <input class="form-control" type="text" name="district" value="<?=$_SESSION['user']->name_district?>" placeholder="Please choise District...">
		                                </div>
		                                <div class="col-lg-6 form-group">
		                                	<label for=""> Place Detail</label>
		                                    <input class="form-control" type="text" name="place_detail" value="<?=$_SESSION['user']->address_detail?>" placeholder="Place detail...">
		                                </div>
		                                <div class="col-lg-6 form-group">
		                                	<label for="">Phone Number</label>
		                                    <input class="form-control" type="text" name="tel" value="<?=$_SESSION['user']->tel?>" placeholder="Phone">
		                                </div>
		                                <div class="col-lg-6 form-group">
		                                	<label for=""> Email</label>
		                                    <input class="form-control" type="text" name="email" value="<?=$_SESSION['user']->email?>" placeholder="Email">
                               			 </div>
                            			<?php
                            		}else{
                            			?>
                         
		                                    <input  class="form-control" type="hidden" name="token" value="<?=$random?>" placeholder="Name">
		                             
                            			<div class="col-lg-6 form-group">
                            			 	<label for="">Name</label>
		                                    <input  class="form-control"type="text" Name="name" placeholder="Name">
		                                </div>
		                                <div class="col-lg-6 form-group">
		                                	<label for="">Province</label>
		                                    <select class="form-control custom-select" name="province" id="province">
											    <option selected>Choose Province ...</option>
												<?php
													foreach ($list_province as $value) {
														?>
														<option value="<?=$value->id?>"><?=$value->name_province?></option>
														<?php
													}
												?>
											  </select>
		                                </div>
		                                <div class="col-lg-6 form-group">
		                                	<label for="">District</label>
		                                    <select class="form-control custom-select" name="district" id="district">
											    <option selected>Choose District ...</option>
											  </select>
		                                </div>
		                                <div class="col-lg-6 form-group">
		                                	<label for=""> Place Detail</label>
		                                    <input class="form-control" Name="place_detail" type="text" placeholder="Phone">
		                                </div>
		                                <div class="col-lg-6 form-group">
		                                	<label for="">Phone Number</label>
		                                    <input class="form-control" Name="tel" type="text" placeholder="Password">
		                                </div>
		                                <div class="col-lg-6 form-group">
		                                	<label for=""> Email</label>
		                                    <input class="form-control" Name="email" type="text" placeholder="Re-Password">
                               			 </div>
                               			 <script>
                               			 	$(document).ready(function(){
                               			 		$("#province").change(function(){
                               			 			var id = $("#province").val();
                               			 			$.get("Ajax/getDistrict.php",{id:id},function(data){
                               			 				$("#district").html(data);
                               			 			});
                               			 		});
                               			 	});
                               			 </script>
                            			<?php
                               		}
                            	?>
                                
                                <div class="col-lg-6 form-group">
                                    <button type="submit" value="submit" style="margin-left: 40%;" class="btn subs_btn form-control" name="order">Order</button>
                                </div>
                            </form>
                        </div>
                        <?php
                        	if(isset($_SESSION['error_order']))
                        	{
                        		echo '<div class="container"><div style="text-align:center;; margin:10px auto;" class="alert alert-danger">'.$_SESSION['error_order'].'</div></div>';
                        	}
                        ?>
                    </div>
                </div>
            </div>
        </section>
            </div>
        </section>+