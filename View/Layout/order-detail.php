<section class="shopping_cart_area p_100">
            <div class="container">
                <div class="form-search" style="width: 100%;margin-bottom: 100px;">
                    <form style="padding: 5px 10px; margin-left: 30%;" class="form-inline">
                      <div class="form-group mx-sm-3 mb-2">
                        <input type="text" class="form-control" id="searchValue" placeholder="email or Your id order ...">
                      </div>
                      <button type="button" id="search" class="btn btn-primary mb-2">Search</button>
                    </form>
                </div>
                <div class="cart_product_list">
                </div>
            <script>
               $(document).ready(function(){
                $("#search").click(function(){
                     var value = $("#searchValue").val();
                    $.get("Ajax/getOrder.php",{value:value},function(data){
                        $(".cart_product_list").html(data);
                    });
                });
               });
            </script>
            <div id="result"></div>